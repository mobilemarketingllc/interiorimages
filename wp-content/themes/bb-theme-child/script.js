(function($) {
    $(function() {
        if(typeof FWP != 'undefined'){
            FWP.loading_handler = function() {
                $(".facet_filters .close_sidebar").click(function(e){
                    e.preventDefault();
                    $(".facet_filters").animate({"left":-$(".facet_filters").outerWidth()-20},500);
                });
                $(".open_sidebar").click(function(e){
                    e.preventDefault();
                    $(".facet_filters").css("left",-$(".facet_filters").outerWidth()-20);
                    $(".facet_filters").animate({"left":0},500);
                });
            }
        }
    });
})(jQuery);